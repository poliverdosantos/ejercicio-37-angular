import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { InicioComponent } from './components/inicio/inicio.component';
import { FotosComponent } from './components/fotos/fotos.component';
import { ProductosComponent } from './components/productos/productos.component';
import { PromocionesComponent } from './components/promociones/promociones.component';
import { ProductosGuard } from './guards/productos.guard';
import { PromocionesGuard } from './guards/promociones.guard';

const routes: Routes = [
  { path:'inicio', component: InicioComponent},
  { path:'fotos', component: FotosComponent},
  { path:'productos',component: ProductosComponent, canActivate: [ProductosGuard] },
  { path:'promociones', component: PromocionesComponent, canActivate:[PromocionesGuard]},
  { path: '**',  redirectTo:'inicio'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
