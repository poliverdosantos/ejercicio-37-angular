import { TestBed } from '@angular/core/testing';

import { PromocionesGuard } from './promociones.guard';

describe('PromocionesGuard', () => {
  let guard: PromocionesGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PromocionesGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
